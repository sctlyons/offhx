﻿using UnityEngine;
using UnityEngine.UI;
public class Suspicion : MonoBehaviour
{
    private Slider slider;

    public float FillSpeed = 0.5f;
    private float targetSuspicion = 0;

    static Suspicion _instance;
    static public Suspicion Instance()
    {
        return _instance;
    }

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(this);
            return;
        }
        _instance = this;

        slider = gameObject.GetComponent<Slider>();
    }
    void Start()
    {

    }
    void Update()
    {   
         if (slider.value < targetSuspicion)
         {
            IncrementSuspicion(0.15f);
            slider.value -= FillSpeed * Time.deltaTime;
         }
    }

    public void IncrementSuspicion(float newSuspicion)
    {
        targetSuspicion = slider.value + newSuspicion;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraViewController : MonoBehaviour
{

    public GameObject cameraOne;
    public GameObject cameraTwo;
    private AudioListener cameraOneAudioLis;
    private AudioListener cameraTwoAudioLis;



    // Start is called before the first frame update
    private void Start()
    {
        cameraOneAudioLis = cameraOne.GetComponent<AudioListener>();
        cameraTwoAudioLis = cameraTwo.GetComponent<AudioListener>();

        cameraPositionChange(PlayerPrefs.GetInt("CamerePosition"));
    }

    // Update is called once per frame
    private void Update()
    {
        switchCamera();
    }

    public void cameraPositionM()
    {
        cameraChangeCounter();

    }

    private void switchCamera()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            cameraChangeCounter();
        }
    }

    private void cameraChangeCounter()
    {
        {
        int cameraPositionCounter = PlayerPrefs.GetInt("CameraPosition");
        cameraPositionCounter++;
        cameraPositionChange(cameraPositionCounter);

        }
    }

    //Camera change logic
    private void cameraPositionChange(int camPosition)
    {
        if (camPosition > 1)
        {
            camPosition = 0;
        }

        PlayerPrefs.SetInt("CameraPosition", camPosition);

        if (camPosition == 0)
        {
            cameraOne.SetActive(true);
            cameraOneAudioLis.enabled = true;

            cameraTwoAudioLis.enabled = false;
            cameraTwo.SetActive(false);
        }
        if (camPosition == 1)
        {
            cameraTwo.SetActive(true);
            cameraTwoAudioLis.enabled = true;

            cameraOneAudioLis.enabled = false;
            cameraOne.SetActive(false);
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
       
    }
}

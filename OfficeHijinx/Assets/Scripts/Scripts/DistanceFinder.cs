﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceFinder : MonoBehaviour
{
    public GameObject Object1;
    public GameObject Player;
    public float Distance_;

    // Update is called once per frame
    private void Update()
    {
        Distance_ = Vector3.Distance(Object1.transform.position, Player.transform.position);

        if (Distance_ < 3)
        {
            print("In Range");
        }
    }
}

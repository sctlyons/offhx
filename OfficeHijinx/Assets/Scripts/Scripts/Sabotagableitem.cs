﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sabotagableitem : MonoBehaviour
{
    public bool isSabotaged = false;

    public bool isPlayerNear = false;

    public ParticleSystem InkSpray;

   
    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            isPlayerNear = true;

        }
        else
        {
            isPlayerNear = false;
        }
        if (other.gameObject.CompareTag("Boss") && (isSabotaged))
        {
            InkSpray.Play();
        }
    }

    void Update()
    {
        if (isPlayerNear)
        {
            if (Input.GetKeyUp("e"))
            {
                isSabotaged = true;
                
            }
        }
        
    }
}

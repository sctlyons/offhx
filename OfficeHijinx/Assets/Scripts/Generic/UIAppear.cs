﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIAppear : MonoBehaviour
{
    [SerializeField] private Image Image;
    [SerializeField] private Text Text;
    public Image progressbarimg;
    public Text timerobj;
    public float totaltime;
    public bool TimerOn;
    //public AudioSource coffeeFinished;


    private void Start()
    {
        TimerOn = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Image.enabled = true;
            Text.enabled = true;
            TimerOn = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Image.enabled = false;
            Text.enabled = false;
            TimerOn = false;
            print("Ayo");
        }
    }

    private void Update()
    {
        if (((totaltime > 0) && TimerOn == true) && Input.GetKey(KeyCode.E))
        {
            totaltime = totaltime - Time.deltaTime;
            timerobj.text = totaltime.ToString("0");
            progressbarimg.fillAmount -= 1.0f / 5 * Time.deltaTime;
        }
        else
        {
            timerobj.text = "5";
        }
        if(totaltime < 0)
        {
            Image.enabled = false;
            Text.enabled = false;
            //coffeeFinished.Play(0);
          
        }
        if (totaltime <= 0)
        {
            Suspicion.Instance().IncrementSuspicion(0.15f);
        }
    }
}

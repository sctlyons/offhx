﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class progressionstatus : MonoBehaviour
{
    public Image progressbarimg;
    public Text timerobj;
    public float totaltime = 20;
    public bool TimerOn;
    public GameObject progressbar;
    private void Start()
    {
        TimerOn = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("coffe"))
        {
            TimerOn = true;
        }
    }

    private void Update()
    {
        if ((totaltime > 0) && TimerOn == true)
        {
            totaltime = totaltime - Time.deltaTime;
            timerobj.text = totaltime.ToString("0");
            progressbarimg.fillAmount -= 1.0f / 20 * Time.deltaTime;
        }
        else
        {
            timerobj.text = "0";
        }
        
    }
}
